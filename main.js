var express = require('express');
var fs = require('fs');


//generator titluri
var terminals = {};
var startwords = [];
var wordstats = {};
var titles = [];

var array = fs.readFileSync(__dirname+"/data/parsed_titluri_prod").toString().split("\n");
for(i in array) {
    titles.push(array[i].toLowerCase());
}

for (var i = 0; i < titles.length; i++) {
    var words = titles[i].split(' ');
    terminals[words[words.length-1]] = true;
    startwords.push(words[0]);
    for (var j = 0; j < words.length - 1; j++) {
        if (wordstats.hasOwnProperty(words[j])) {
            wordstats[words[j]].push(words[j+1]);
        } else {
            wordstats[words[j]] = [words[j+1]];
        }
    }
}

function choice(a) {
    var i = Math.floor(a.length * Math.random());
    return a[i];
} 

var make_title = function (min_length) {
    word = choice(startwords);
    var title = [word];
    while (wordstats.hasOwnProperty(word)) {
        var next_words = wordstats[word];
        word = choice(next_words);
        title.push(word);
        if (title.length > min_length && terminals.hasOwnProperty(word)) break;
    }
    if (title.length < min_length) return make_title(min_length);
    return title.join(' ');
};






//###########SERVER
var app = express();

app.get("/",function(req,res){
	res.sendFile(__dirname+"/index.html");
});

app.get("/generate",function(req,res){
	res.send(make_title(3 + Math.floor(3 * Math.random())));
});

app.set('port', process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 3002);
app.set('ip', process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1");

var server = app.listen(app.get('ip'), function(){
	console.log("✔ Express server listening at %s:%d ", app.get('ip'),app.get('port'));
});





